#include <stdio.h>
int main()
{
  int array[10], search, i ;

  printf("Enter 10 integer(s)\n");

  for (i = 0; i < 10; i++)
    scanf("%d", &array[i]);

  printf("Enter a number to search\n");
  scanf("%d", &search);

  for (i = 0; i < 10; i++)
  {
    if (array[i] == search)    
    {
      printf("%d is present at location %d.\n", search, i+1);
      break;
    }
  }
  if (i == 10)
    printf("%d isn't present in the array.\n", search);

  return 0;
}