#include<stdio.h>
int read()
{
	int x;
	printf("enter the coordinate: \n");
	scanf("%d",&x);
	return x;
}
int check(int x,int y)
{
	int A;
	if(x==0&&y==0)
	A=1;
	else if(x>0&&y>0)
	A=2;
	else if(x<0&&y>0)
	A=3;
	else if(x<0&&y<0)
	A=4;
	else
	A=5;
    return A;
}
void display(int A)
{
	if(A==1)
	printf("the point lies on origin");
	else if(A==2)
	printf("the point lies in first quadrant");
	else if(A==3)
	printf("the point lies in second quadrant");
	else if(A==4)
	printf("the point lies in third quadrant");
	else
	printf("the point lies in fourth quadrant");
}
int main()
{
	int x,y,q;
	x=read();
	y=read();
	q=check(x,y);
	display(q);
	return 0;
}
	
