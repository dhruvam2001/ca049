#include<stdio.h>
void swap(int *p,int *q);
int main()
{
    int x,y;
    printf("enter two numbers to be swapped:");
    scanf("%d %d",&x,&y);
    printf("numbers before swapping, x=%d and y=%d\n",x,y);
    swap(&x,&y);
    printf("numbers after swapping, x=%d and y=%d",x,y);
    return 0;
}
void swap(int *p,int *q)
{
    int temp;
    temp=*p;
    *p=*q;
    *q=temp;
}